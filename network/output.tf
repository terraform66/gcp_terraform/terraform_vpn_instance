output "external-ip-address" {
  value = google_compute_address.external-ip-address.address
}

output "internal-ip-address" {
  value = google_compute_address.internal-ip-address.address
}

output "vpc-network-vpn-v2ray" {
  value = google_compute_network.vpc-network-vpn-v2ray
}