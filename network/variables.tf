variable "my-project" {
  type = string
  default = "myweb-frei"
}

variable "my-region" {
  type = string
  default = "europe-central2"
}

variable "my-zone" {
  type = string
  default = "europe-central2-a"
}