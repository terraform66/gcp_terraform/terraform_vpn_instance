provider "google" {
  version = "3.20.0"
  credentials = file("credentials.json")
  project = var.my-project
  region = var.my-region
  zone = var.my-zone
}

terraform {
  backend "gcs" {
    bucket = "tf-state-prod"
    region = var.my-region
    prefix = "terraform/state"
    key = "dev/network/terraform.tfstate"
  }
}

resource "google_project_service" "api" {
  for_each = toset([
    "cloudresourcemanager.googleapis.com",
    "computegoogleapis.com"
  ])
  disable_on_destroy = false
  service = each.value
}

resource "google_compute_network" "vpc-network-vpn-v2ray" {
  name = "vpn-network"
}

resource "google_compute_address" "external-ip-address" {
  name = "external-ip-address"
}

resource "google_compute_subnetwork" "vpc-subnetwork-vpn-v2ray" {
  name = "vpn=subnet"
  ip_cidr_range = "10.0.0.0/16"
  region = var.my-region
  network = google_compute_network.vpc-network-vpn-v2ray.id
}

resource "google_compute_address" "internal-ip-address" {
  name = "internal-ip-address"
  subnetwork = google_compute_subnetwork.vpc-subnetwork-vpn-v2ray.id
  address_type = "INTERNAL"
  address = "10.0.33.33"
}

