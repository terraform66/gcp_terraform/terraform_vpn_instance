provider "google" {
  version = "3.20.0"
  credentials = file("credentials.json")
  project = var.my-project
  region = var.my-region
  zone = var.my-zone
}

terraform {
  backend "gcs" {
    bucket = "tf-state-prod"
    region = var.my-region
    prefix = "terraform/state"
    key = "dev/compute/terraform.tfstate"
  }
}

data "terraform_remote_state" "network" {
  backend = "gcs"
  config = {
    bucket = "tf-state-prod"
    region = var.my-region
    prefix = "terraform/state"
    key = "dev/network/terraform.tfstate"
  }
}

data "google_compute_image" "vpn_image" {
  family  = var.family
  project = var.project-family
}

resource "google_project_service" "api" {
  for_each = toset([
    "cloudresourcemanager.googleapis.com",
    "computegoogleapis.com"

  ])
  disable_on_destroy = false
  service = each.value
}


resource "google_compute_firewall" "vpn" {
  name = "vpn_access"
  network = data.terraform_remote_state.network.outputs.vpc-network-vpn-v2ray
}

resource "google_compute_instance" "vpn-instance" {
  name = "vpn-instance"
  machine_type = var.machine-type
  boot_disk {
    initialize_params {
      image = data.google_compute_image.vpn_image.self_link
    }
  }

  network_interface {
    network = data.terraform_remote_state.network.outputs.vpc-network-vpn-v2ray
    access_config {
      nat_ip = data.terraform_remote_state.network.outputs.external-ip-address
    }
  }
  metadata_startup_script = <<EOF
  #!/bin/bash
apt update -y
apt install apache2 -y
MYIP='curl http://169.254.169.254/latest/meta-data/local-ipv4'
echo "<h2> Webserver with privateIP: $MYIP</h2><br>Build by Terraform external file" > /var/www/index.html
systemctl restart apache2
EOF

  depends_on = [google_project_service.api, google_compute_firewall.vpn]
}
