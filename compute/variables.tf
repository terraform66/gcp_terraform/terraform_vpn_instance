variable "my-project" {
  type = string
  default = "myweb-frei"
}

variable "my-region" {
  type = string
  default = "europe-central2"
}

variable "my-zone" {
  type = string
  default = "europe-central2-a"
}

variable "family" {
  type = string
  default = "debian-9"
}

variable "project-family" {
  type = string
  default = "debian-cloud"
}

variable "machine-type" {
  type = string
  default = "f1-micro"
}